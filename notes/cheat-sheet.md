# Einleitung

# Über mich

# Syntax

```typescript
let myName: string
myName = 'Marco'
myName = 4
```

# Any

```typescript
let myName
```

# Types vs. Interfaces

```typescript
type Age = number
type Movie = {
    id: string
    title: string
    ageRating: number
    genre: string
}
```

# Literal Types

```typescript
const marcosName = 'Marco'
let marco: 'Marco'
```

# Union Types

```typescript
type AgeRating = 0 | 6 | 12 | 16 | 18
type Genre = 'Horror' | 'Romance' | 'Action'
```

# Template Literal Types

```typescript
type Id = `M-${AgeRating}-${number}`
```

# Utility Types

```typescript
type ChildAgeRating = Exclude<AgeRating, 16 | 18>
type MovieWithoutId = Omit<Movie, 'id'>
type PartialMovie = Partial<Movie>
```

# Generic und Conditional Types

## Generic

```typescript
type LoaderState<T> = { state: 'loading' | 'error' | 'success', value: T }
```

## Conditional

```typescript
let a: 'hello' extends string ? true : number
type AsArray1<T> = T[]
type AsArray2<T> = T extends any ? T[] : never 
type FlatArray<T> = T extends unknown[] ? T : T[]
```

## Beispiel

```typescript
function getAgeRatings(): AgeRating[] {
    return [0, 6, 16, 18]
}

type AllCombinationsOf<T, S = T> = [T] extends [never]
  ? []
  : T extends S
    ? [T, ...AllCombinationsOf<Exclude<S, T>>]
    : never
```

# Mapped Types

```typescript

type Icon = `<svg>${string}</svg>`
const icons = {
  horror: Icon,
  romance: Icon,
  action: Icon,
}

function showIcon(genre: Genre) {
const icon = icons[genre]
}
```

# Ausblick

```typescript
type FlatArray<T> = T extends (infer R)[] ? FlatArray<R> : T[]
let icons: {
  [key in Genre as k extends 'Horror' ? never : Uncapitalize<k>]: string
}
```

# Ende