# TypeScripts Power Level? It's over 9000!!!

## Einleitung

Willkommen zu meinem Vortrag zur Typisierung in TypeScript. Ich habe heute vor, euch in die Tiefen der
TypeScript-Möglichkeiten zu führen (und dort hoffentlich nicht zu verlieren). Ich zeige euch, was alles in TypeScript
drin steckt, warum es sich zu meiner liebsten Programmiersprache gemausert hat und wie auch ihr vielleicht eure Projekte
noch weiter ausgestalten könnt. Oder aber, wenn ihr noch nicht viel mit TypeScript gemacht habt, warum es sich lohnen
kann, mal einen zweiten Blick darauf zu werfen.

Auch für überwiegend Backendentwickler kommt hier hoffentlich der Anreiz oder wenigstens die Erkenntnis, dass man auch
im Frontend mittlerweile sauberen, typisierten Code schreiben kann. Und da sogar teilweise weiter gehen kann, als man es
sich in Sprachen wie Java nur erträumen kann. Wobei TypeScript ja auch immer häufiger im Backend eingesetzt wird. Gerade
Microservices oder Serverless bietet sich dazu einfach an. Aber das soll heute nicht das Thema sein, heute geht es
ausschließlich um die Typisierung.

## Syntax, erster Typ

Daher, falls auch Zuhörende anwesend sind, die bisher noch nicht mit TypeScript gearbeitet haben, will ich ganz kurz die
relevante Syntax für diesen Kurs einführen.

```typescript
let a: string
```

Hierdurch lege ich eine Variable namens a vom Typ string an. Man beachte also: Doppelpunkt leitet den Typen ein. Ich
kann auch direkt noch einen Wert zuweisen:

```typescript
let a: string = 'Hallo'
```

In diesem Fall könnte ich mir auch die Typspezifizierung sparen, TypeScript leitet den Typ dennoch korrekt aus dem
zugewiesenen Wert ab:

```typescript
let a = 'Hallo'
```

Neben string existieren natürlich noch weitere Typen wie number, boolean, arrays, Klassen, Date, any, null, undefined,
unknown, never, ... Höh, moment, "any", "null", "undefined", "unknown", "never"? Was zum Teufel (fragt sich der
nicht-TypeScript-versierte Entwickler)? Keine Angst, das gucken wir uns alles noch genauer an, was es damit auf sich
hat.

Wenn man in TypeScript eigene Typen definieren möchte, dann kann man das durch Interfaces oder Types tun.
(Klassen gehen natürlich auch, haben aber auch in der Regel immer schon eine zugehörige Logik. Und Logik ist wirklich
das letzte, was wir uns in diesem Vortrag angucken wollen, wir wollen uns ganz auf die Typen konzentrieren. Daher
vergessen wir Klassen jetzt erstmal. Aber natürlich kann ich am Ende auch Klassen schreiben, die zuvor deklarierete
Typen oder Interfaces implementieren. Aber zurück zum roten Faden)

Interfaces und Typen sind sehr ähnlich, können identisch sein, können aber auch sehr unterschiedlich sein. Sind aber
beide dazu da, um einen Typen zu definieren. *Same but different Meme*
Der Hauptunterschied ist, dass Types auch als Aliase dienen können sowie auch als Kombination von verschiedenen anderen
Typen (das Stichwort ist hier Union Types, aber dazu kommen wir gleich genauer. Muss man jetzt noch nicht aufschreibe,
wird auch keine Testfrage). Interfaces hingegen können via Interface-Merging an mehreren Stellen definiert werden und
werden dann am Ende zusammengeführt. Das geht bei Types nicht. Interfaces können auch Types erweitern, Types können
Interfaces erweitern, KLassen können beides implementieren (alles nur, solange die Types keine Union Types beinhalten).
Types sind in der Regel daher deutlich flexibler und wir werden uns heute auch ausschließlihc mit Types beschäftigen.

Also definieren wir uns mal einen Typen. Das sieht in TypeScript so aus:

```typescript
type Book = {
    title: string
    id: string
    section: string
    author: string
}

type Movie = {
    id: string
    title: string
    ageRating: number
}
```

Achtung: Hier steht ein =, obwohl wir hier mit Typen und nicht mit Werten arbeiten. Aber das was links und rechts vom =
steht sind beides Typesn, es ist also wieder eine gleichwertige Zuordnung und wir definieren nicht den Typen einer
Variable.

Wenn wir jetzt eine Variable mit einem Objekt eines dieser Typen anlegen, dann tun wir das ganz analog:

```typescript
let book: Book
```

Und wenn ich einen Wert zuweisen möchte, dann unterstützt mich meine IDE dabei:

```typescript
let book: Book = {
    title: 'Clean Code',
    section: 'dfs',
    id: 'sdfds',
    author: 'dfd'
}
```

Wenn ich hier die Typspezifikation wegnehme, dann erkennt TypeScript nicht, dass die Variable vom Typ Book ist; es
könnte ja sein, dass der Wert nur zufällig dieselbe Struktur hat. Wenn ich die Zuweisung also entferne, sieht man, das
Typescript jetzt einen anonymen Typen zuweist. *zeigen*
Genauso kann ich auch eine Variabel von einem anonymen Typen definieren, wenn ich ihn zum Beispiel nur einmalig brauche.

```typescript
let person: {
    name: string
    age: number
} = {
    name: 'hans',
    age: 12
}
```

Sollte man vorsichtig tun, damit es nicht zu unübersichtlihc und unleserlich wird, kann manchmal aber ganz nützlich
sein.

Okay, soweit zur einfachen Syntax. Bisher nichts neues, alles good old typings und die ersten fragen sich, warum sie
hier sind. Zeit also, langsam die spannendnen Dinge herauszunholen. Damit das ganze wenigstens den Anschein eines roten
Fadens hat, bleiben wir doch bei den Book- und Movie-Typen und gucken mal, was wir damit noch so weiteres anstellen
können.

## Literal Types, Union Types, Intersection Types

Machen wir also erstmal einen kleinen Exkurs ;-)
Wenn ich eine Variable anlege und ihr direkt einen Wert zuweise, dann versucht TypeScript, den Typen der Variable vom
zugewiesenen Wert abzuleiten:

```typescript
let text = 'Hallo'
```

Text ist vom Typ string, weil TypeScript erkannt hat, dass der Wert 'Hallo' zugewiesen wurde, was ja offentsichtlihc ein
String ist. Dementsprechend kann ich in weiteren Zuweisungen auch nur weitere Strings zuweisen. Bei etwas anderem als
String meckert TypeScript:

```typescript
let text = 'Hallo'
text = 'Tschüss'
text = 12
```

Was passiert jetzt, wenn ich `text` nicht als let, sondern als const anlege?

```typescript
const text = 'Hallo'
```

Wenn wir uns jetzt den Typen angucken, sehen wir was komisches. Der Typ ist "Hallo" (bedenke: Doppelpunkt gibt den Typ
an. Hier steht also wirklich der Typ nd nicht der Wert). TypeScript hat erkannt, dass text nie einen anderen Wert
annehmen kann und leitet den Typen daher so streng wie möglich ab. Und noch spezifischer als der Typ string ist der Typ,
der lediglich den Wert 'Hallo' zulässt. Ein so genannter "Literal Type". Das funktioniet übrigens nicht nur bei Strings
sondern auch bei number, boolean und allen andern Typen, deren Wert man im Code einfach darstellen kann.

Wir können das auch von Hand tun, wenn wir nicht mit const arbeiten:

```typescript
let text: 'Hallo'
text = 'Hallo'
text = 'Tschüss'
```

Wir können TypeScript übrigens auch bei der automatischen Typeableitung dazu zwingen, den Typen so streng wie möglich
abzuleiten. Dazu benutzen wir 'as const'. Das macht in diesem Beispiel wenig Sinn, aber wenn man zum Beispiel ein Tupel
anlegen möchte und TypeScript auch den Tupel-Typen ableiten soll statt dem allgemeineren Array-Typen, kann das sinnvoll
sein. Dazu aber später noch ein Beispiel.

```typescript
let text = 'Hallo' as const
text = 'Tschüss'
```

Das 'as const' bezieht sich hier auf den Typen, nicht auf den Wert. Den kann ich auch nachträglich noch ändern. Naja, in
dem Fall macht das nur wenig Sinn, denn der einzige Wert, der zuweisbar ist, ist ja bereits zugewiesen. In der Regel
machen Literal Types alleine daher wenig Sinn. Wie können wir die Sache dann verbessern? Es wäre dohc nett, wenn ich
sagen könnte, text soll entweder 'Hallo' oder 'Tschüss' sein, sonst aber nichts. Und das geht mit sogenannten Union
Types:

```typescript
let text: 'Hallo' | 'Tschüss'
text = 'Hallo'
text = 'Tschüss'
text = 'Anderer String'
```

Union Types werden durch den Oder-Operator beschrieben. Es können beliebig viele Typen verodert werden. Es ist auch
möglich, prinzipiell unvereinbare Typen zu verodern, ich könnte also eine Variable anlegen, die entweder string oder
number ist. Warum heißt das Union Type? Wenn wir uns alle Werte, die dem ersten Type zugewiesen werden können als Menge
vorstellen, und als zweite Menge alle Werte, die dem zweiten Typen zugewiesen werden können, dann ergibt die
Vereinigungsmenge genau die Werte, die dem Union Type zugewiesen werden können.

Die Mengentheoretiker unter Ihnen fragen sich jetzt sicher: Wenn es einen Typen für die Vereinigungsmenge gibt, gibt es
dann auch einen für die Schnittmenge? Und natürlich ist die Antwort "Ja".

```typescript
type T1 = 'A' | 'B' | 'C' | 'D' | 'E'
type T2 = 'X' | 'V' | 'D' | 'A' | 'Z'

let char: T1 & T2
char = 'A'
char = 'B'
char = 'D'
```

Nützlicher wird das, wenn wir Objekttypen verunden:

```typescript
type ObjectWithId = { id: string }
type User = { username: string, age: number }
type UserInDb = User & ObjectWithId

let user: UserInDb
user = {
    id: 'userId',
    age: 12,
    username: 'Hannes'
}
```

In diesem Falle könnte man sagen, UserInDb ist ein Typ, der die Typen ObjectWithId und User extended (und genauso würde
man in der Type-Schreibweise auch Typen extenden).

So, erinnert sich noch jemand an den roten Faden? Wir hatten Books und Movies. Wie könnten wir jetzt hier Literal Types
verwenden?

Gucken wir uns mal den Moie-Typen an. Wir haben hier ein Age Rating. Mindestens die Kinder unter Ihnen wissen, dass es
in Deutschland nicht beliebige Altersfreigaben für Filme gibt. Und Altersfreigaben für 13,4225 Jahre erst recht nicht.
Das wäre momentan aber alles möglcih. Optimieren wir as also mal:

```typescript
type Movie = {
    id: string
    title: string
    ageRating: 0 | 6 | 12 | 16 | 18
}
```

Viel besser. Und weil wir den Typen einerseits vielleicht noch an anderer Stelle gebrauchen könenn und weil wir ihn
außerdem mit einer Semantik belegen wollen, extrahieren wir ihn doch mal als benannten Typen:

```typescript
type AgeRating = 0 | 6 | 12 | 16 | 18
```

Auch hier wieder: Hier steht zwar ein gleich, aber links davon steht ein Typ, das heißt auch, dass die Zahlen rechts
davon keine Werte sondern Typen sind.

Und schon haben wir einen strikteren Typen und stellen sicher, dass wir nicht versehentlich irgendwo ein ageRating von
13 zuweisen (solange wir nicht plötzlihc mit anys um uns werfen; die machen alles kaputt und wer aktiv any benutzt, der
hat die Kontrolle über sein Leben verloren ;-) *Karl Lagerfeld*.

*Bis hierhin: 15 Minuten*

## Template Literal Types

Wenden wir uns als nächstes der id zu. Gehen wir mal davon aus, dass die IDs nicht irgendwelche Random Strings sind,
sondern einem bestimmten Schema folgen:
IDs von Filmen beginnen immer mit einem 'M', gefolgt von einem Bindestrich, dem Age Rating, einem Bindestruch und einer
Nummer. Also zum Beispiel 'M-12-341451'. Wir könnten jetzt natürlich einen Union Type schreiben, der sämtliche Literal
Types von M-0-0 bis M-18-999999999 beinhaltet (wenn das denn überhaupt reicht). Allerdings gibt unser Laptop vermutlich
auf, bevor wir sämtliche 5 Milliarden literal string types eingegeben haben. Wir brauchen also eine effizientere Art und
Weise dazu.

Und Glücklicherweise bietet TypeScript auch diese Möglichkeit, das Stichwort ist Template Literal Type. Wir können der
ID folgenen Typen geben:

```typescript
type MovieId = `M-${AgeRating}-${number}`
```

Die Syntax entspricht einem template string, nur dass wir hier eben Typen einsetzen können. Jeder Wert, der jetzt dem
oben angegebenen String-Schema entspricht, kann diesem Typen zugewiesen werden:

```typescript
let movieId: MovieId
movieId = 'M-0-1234'
movieId = 'M-12-2247398473298'
movieId = 'B-12-123213'
```

(Dass hier auch Kommazahlen für die Zahl am Schluss möglich sind, ignorieren wir jetzt einfach mal).

Bei Büchern sollte die ID aus 'B', der Abteilung und einer Nummer bestehen:

```typescript
type BookId = `B-${string}-${number}`
```

Keine Angst, wir kümmern uns da auch gleich noch weiter drum.

Nur der Vollständigkeit halber: Natürlich können Template Literal Types genauso verundet oder verodert werden, wie jeder
andere Typ auch:

```typescript
type BeginsWithDash = `-${string}`
type EndsWithDash = `${string}-`
type BeginsAndEndsWithDash = BeginsWithDash & EndsWithDash

let text: BeginsAndEndsWithDash
text = '-test-'
```

## Utility Types

Ah, ich höre wieder die berüchtigten Mengentheoretiker von vorhin aufbegehren: Schnittmenge und Vereinigungsmenge, schön
und gut, aber was ist mit Differenzmenge? Die wurde uns sogar auf dem Teaserbild des Abschnitts versprochen!
Keine Sorge, liebe Kollegen, auch das ist natürlich möglich. Dazu müssen wir uns an den Utility Types bedienen.

Utility Types sind Hilfstypen, welche von TypeScript direkt zur Verfügung gestellt werden, um bestehende andere Typen
etwas abzuändern.

Zum Beispiel gibt es dann Exclude, welches zwei Union Typen übernimmt und einen neuen Typen ergibt, welcher nur die Werte
zulässt, die ausschließlich im ersten Typen enthalten sind.
Kleines Beispiel:

```typescript
type DivisibleByTwo = 0 | 2 | 4 | 6 | 8
type DivisibleByThree = 0 | 3 | 6 | 9

type OnlyDivisibleByTwo = Exclude<DivisibleByTwo, DivisibleByThree>
```

Auf Objekt-Property-Ebene gibt es etwas ähnliches mit Omit:

```typescript
type MovieWithoutTitle = Omit<Movie>
```

Genauso kann man aber auch alle Properties optional machen, erzwingen oder auch auf string literal types agieren.

```typescript
type PartialMovie = Partial<Movie>
type RequiredSomething = Required<{ name: string, age?: number }>
type UpperCaseGreeting = Uppercase<'Hello World'>
type CapitalizedName = Capitalize<'hannes'>
```

Man kann sich online angucken, was es alles für Utility Types gibt, vielleicht ist ja was dabei, was der ein oder andere
hier im Publikum in seinem Projekt auch gut gebrauchen könnte.

## Generic und Conditional Types

Aber was macht man, wenn man zwar theoretisch einen Utility Type gebrauchen könnte, es aber genau den Typ für die eigenen
Anforderungen nicht gibt?
Einfache Frage, wir sind ja Entwickler. Wir schreiben ihn einfach selbst. So ein Utility Type ist ja nichts anderes
als ein generischer Typ, wie man ihn auch aus anderen Sprachen schon kennt.
Wie sehen also generische Typen in TypeScript genau aus, wie legt man diese an? Ein ganz einfaches Beispiel wäre
zum Beispiel:

```typescript
type LoaderState<T> = { state: 'loading' | 'error' | 'success', value: T }
```

Analog zu anderen Sprachen kann auch hier der generische Parameter restricted werden, wir können zum Beispiel nur Typen zulassen, die string extenden:
```typescript
type LoaderState<T extends string> = { state: 'loading' | 'error' | 'success', value: T }
```

Ok, da muss ich nicht weiter drauf eingehen. Worauf ich aber eingehen möchte, sind conditional types. Mit diesen werden generische Typen nämlich extrem mächtig.

Gucken wir uns conditional types aber erstmal separat davon an:

```typescript
let a: 'hello' extends string ? true : number
```

Oh gott! Oh gott! Was steht denn hier?
Ganz einfach: wir legen natürlich eine Variable names a an und spezifizieren dann ihren typen. Und was ist das für ein Typ?
Falls (!) der Type 'hello' eine Erweiterung des Typen string ist, dann hat die Variable a den Typ true (ein literal type, a kann dann nur den Wert true erhalten).
Falls der Typ 'hello' aber nicht eine Erweiterung des Typen string ist, dann hat a den Typen number.

'hello' ist eine Erweiterung, daher ist a true. 'hello' ist aber zum Beispiel keine erweiterung von number, gucken wir also was dann passiert:

```typescript
let a: 'hello' extends string ? true : number
a = true
a = 12
```

Gut, verstanden. Aber irgendwie ist es ja Blödsinn. An der Stelle, an der ich das hier geschrieben habe, wusste ich ja bereits, ob die Bedingung erfüllt ist oder nicht.
Warum also nicht direkt true oder number zuweisen?
Ja, an dieser Stelle macht es wirklich wenig Sinn. Aber jetzt kommen wir zurück zu den Generics und gucken mal, was raus kommt, wenn wir die beiden verknüpfen:

```typescript
type IdPrefix<T extends Book | Movie> = T extends Book ? 'B' : 'M'

let bookPrefix: IdPrefix<Book>
let moviePrefix: IdPrefix<Movie>

bookPrefix
moviePrefix
```

Und natürlich können wir das auch nesten (auch wenn es dann schnell unübersichtlihc wird).

```typescript
type AudioBook = {id: string}

type IdPrefix<T extends Book | Movie | AudioBook> = T extends Book ? 'B' : T extends Movie ? 'M' : 'A'

let bookPrefix: IdPrefix<Book>
let moviePrefix: IdPrefix<Movie>

bookPrefix
moviePrefix
```

Noch ein Punkt, der gut zu wissen ist und manchmal zunächst unerwartetes Verhalten erklären kann:
Durch Conditional Types werden Union Types distributiv.
Ich glaube, das kann ich einfach so stehen lassen, da sollte jedem klar sein, was das bedeutet.

Natürlich nicht. Was bedeutet das also? Gucken wir uns das an einem kleinen Beispiel an:

```typescript
type AsArray1<T> = T[]
type AsArray2<T> = T extends any ? T[] : never
```
Die beiden Typen sehen auf den ersten Blick so aus, als ob sie genau dasselbe tun - wenn auch der zweite ziemlich umständlich. Aber wenn wir uns dann mal angucken, was
tatsächlich für Typen am Ende dabei herauskommen, dann sehen wir einen Unterschied:

```typescript
type A1 = AsArray1<AgeRating>
type A2 = AsArray2<AgeRating>
```

Der Conditional Type hat also den Union Type zunächst aufgelöst, jeden Teil einzeln betrachtet und am Ende wieder alles zusammengeführt.
Das kann oft ein gewünschtes verhalten sein, zum Beispiel hier:

```typescript
type AsGreeting<T extends string> = T extends `Hello ${string}` ? T : `Hello ${T}`
```

Wenn das mal nicht gewünscht ist, kann man das allerding unterbinden, indem man die Typen in [] steckt:
```typescript
type AsArray3<T> = [T] extends [any] ? T[] : never
type A3 = AsArray3<AgeRating>
```

Das sollte man im Hinterkopf haben, ich komme später auch nochmal darauf zurück.

## BookSection

Schön und gut, und was können wir damit jetzt tun? Wir können uns mal der Abteilung der Bücher zuwenden. Sagen wir mal, unsere Abteilungen bestehen immer aus zwei Großbuchstaben.
Wenn wir das sicher typisieren wollen, könnten wir jetzt so vorgehen:

```typescript
type Letter = 'A' | 'B' | 'C' | ...
type Section = `${Letter}${Letter}`
```

Prinzipiell keine schlechte Idee, aber etwas lästig zu tippen. Vielleicht können wir das etwas vereinfachen. (in der Realität würde man das einfach ganz sein lassen, aber wir ziehen es jetzt durch).

```typescript
type CharOf<T extends string> = T extends `${infer HEAD}${infer TAIL}` ? HEAD | CharOf<TAIL> : never
```

Ok, zwei neue Dinge sind dazu gekommen: infer und Rekursion (und never; aber das würde ich mal aufschieben. Wen es stört, der kann hier stattdessen einen empty string nehmen. Ist dann zwar nicht ganz das gleiche, aber am never will ich mich gerade nicht aufheben. Fragen dazu aber gerne am Ende).
Wir haben wieder einen Conditional Generic Type, der string-Typen annimmt. 
Wenn sich der übergeben Typ darstellen lässt als `${irgendein Typ}${irgendein anderer Typ}`, dann tun wir X und sonst geben wir never zurück (auf X gehe ih gleich ein).
Dieses 'infer' tut eben genau das. Es guckt, ob es einen Typen gibt, der an diese Stelle gesetzt werden kann, sodass die Bedingung erfüllt ist.
Falls ja, dann können wir (in der true-Expression) auch auf diesen Typen zugreifen. In diesem FAll prüfen wir, ob T mit einem String beginnt, gefolgt von einem weiteren String.

Kleineres Beispiel:
```typescript
type TypeInArray<T extends unknown[]> = T extends (infer S)[] ? S : never
let a: TypeInArray<number[]>
let b: TypeInArray<Book[]>
```

Am Ende muss natürlich nicht never stehen, da kann was ganz beliebiges stehen; ist ja ein normaler conditional type.
In diesem Fall macht never Sinn, da wir durch die Bedingugn für T schon sicher gestellt haben, dass wir imme rin den True-Fall laufen.

Zurück zum obigen Fall, dort inferen wir sogar zwei Typen. Ist aber kein Problem, funktioniert genauso. Gucken wir uns mal die Fälle an:
T:
string  => Evaluiert zu false, da kein allgemeingültiges 'HEAD' und 'TAIL' infered werden. Wir erhalten never
'' => Evaluiert zu false. Es kann zwar ein HEAD inferred werden (der leere String), aber kein Tail mehr. (theoretisch könnte TAIL auch der leere String sein, aber da ist TypeScript schlau genug, das nicht zu beachten.). Wir erhalten never
'a' => Evaluiert zu true. HEAD ist 'a', TAIL ist der leere string. Wir geben also 'a' | CharOf<''> zurück (= 'a' | never = 'a')
'ab' => Evaluert zu true. HEAD ist 'a', TAIL ist 'b'. Wir geben also 'a' | CharOf<'b'> zurück (='a' | 'b')
usw.

Wir können uns also etwas Tipparbeit sparen, indem wir sagen

```typescript
type Letter = CharOf<'ABCDEFGHIJKLMNOPQRSTUVWXYZ'>
```

Insgesamt also 
```typescript
type Section = `${Letter}${Letter}`
type BookId = `B-${Section}-${number}`

type Book = {
    title: string
    author: string
    id: BookId
    section: Section
}
```

Ok, zugegeben, ist vielleicht etwas arbiträr (wenn auch nicht komplett unsinnig). Gucken wir uns ein zweites Beispiel an, was wir mit generischen
und conditional Types tun können.

## AllValuesOf

Stellen wir uns vor, wir haben ein Webinterface, in welchem Bibliothekare einen neuen Film einpflegen können. Für die
Altersfreigabe möchten wir eine Selectbox anbieten, in der er zwischen allen existierenden Altersfreigaben wählen kann.
Wir haben daher irgendwo in unserem Code eine Funktion

```typescript
function getAgeRatings(): AgeRating[] {
    return [0, 6, 16, 18]
}
```

Und hier sehen wir schon eine Kleinigkeit, die wir noch optimieren können. Vielelicht ist dem ein oder anderen schon
aufgefallen, dass hier die 12 vergessen wurde. Durch die Typisierung auf AgeRating-Array haben wir es zwar geschafft,
dass nicht versehentlich eine 10 im Ergebnisarry landen kann, aber es ist dennoch möglich, einzelne Werte zu vergessen
(oder auch doppelt aufzuführen).

Schreiben wir also einen Typen dafür, der das Problem löst:

```typescript
type AllCombinationsOf<T, S extends T = T> = [T] extends [never]
    ? []
    : T extends S
        ? [T, ...AllCombinationsOf<Exclude<S, T>>]
        : never
```

Was passiert hier? Wir haben einen rekursiv definierten, generischen Typen. Er nimmt zwei Parameter T und S, wobei S eine
Extension von T sein muss und per Default auf T gesetzt wird; wir fassen S auch nicht weiter an, es ist bloß ein Hilfstyp
den wir für die Rekursion brauchen.

Zuerst gucken wir, ob T never ist. Wir müssen dazu aber die distribution des conditional types ausschalten. Warum?
Nun, never entspricht der leeren Vereinigung (Type | never = Type). Der Conditional Type möchte die Condition jetzt
für jeden Typen der Vereinigung testen.
Never besteht aber aus keinen Typen, die Condition kann also nicht geprüft werden und das Ergebnis würde immer 'never' sein.

Daher deaktivieren wir die distribution und prüfen konkret, ob T never ist. Das ist unsere Abbruchbedingung, in diesem Falle
geben wir ein leeres Tupel zurück.

Ist T aber nicht never, dann prüfen wir, ob T S extended. Dies wird bei uns immer der Fall sein, da wir S mit T initialisieren.

Und was tun wir dann? Wir geben ein Tupel zurück, in dem ganz vorne T steht und dahinter, gespreadet, alle Werte aus
dem Tupel, das wir erhalten, wenn wir AllCombinationsOf aufrufen mit dem demselben Input, nur dass wir T dort excluded haben.

Jetzt fragt man sich womöglich, warum diese Rekursion nicht direkt nach einem Durchlauf abbricht. Wenn S = T gesetzt wird
und anschließend verwenden wir Exclude<S, T>, dann kommt ja schon never bei raus.

Hier muss man sich jetzt wieder daran erinnern, dass bei conditional Types der union Type distributiv wird. Das heißt,
innerhalb dieses Ausdrucks ist T nicht mehr 0 | 6 | 12 | ..., sondern jeweils ein konkreter Wert davon
(und nur die Endergebnisse werden wieder vereinigt). Das heißt, tatsächlich steht hier dann:

```typescript
// AllCombinationsOf<0 | 6 | 12> = [0, ...AllCombinationsOf<6 | 12>] | [6, ...AllCombinationsOf<0 | 12>], [12, ...AllCombinationsOf<0 | 6>]
// = [0, 6, ...AllCombinationsOf<12>] | [0, 12, ...AllCombinationsOf<6>] | ...
```

Und genau das ist auch der Grund, weswegen wir überhaupt diese tautologische Bedingugn T extends S brauchen: Wir wollen
die Aufsplittung an dieser Stelle haben und somit sämtliche Permutationen der möglichen Werte für AgeRating erhalten.

```typescript
function getAgeRatings(): AllCombinationsOf<AgeRating> {
    return [0, 6, 16, 18]
}
```

Jetzt sehen wir auch sofort, dass hier ein Fehler vorliegt.

## Indexed Type

Gucken wir uns noch ein Beispiel an, das ich (leicht abgewandelt) auch tatsächlich so im Projekt verwende. Außerdem lernen wir direkt noch, was indexed Types sind.

Wir nehmen an, wir würden auf der Webseite der Bibliothek gerne verschiedene SVG Icons verwenden. Die Icons selbst haben wir in eine globale Konstante geschrieben,
um eine zentrale Konfigurationsstelle dafür zu haben:

```typescript
const icons = {
    actionMovie: '<svg>...</svg>',
    romanticMovie: '<svg>...</svg>',
    crimeMovie: '<svg>...</svg>'
} 
```

Außerdem haben wir eine Komponente, welche, gegeben ein Iconname, dieses Icon auf der Webseite darstellen soll. Der Einfachheit halber sagen wir also,
wir haben eine Methode

```typescript
declare function showIcon(iconName: string)
```

Wir sehen direkt das Problem: Dort, wo die Methode aufgerufen wird, müssen wir den exakten IconNamen kennen, bei Tippfehlern fällt es erst auf, wenn auf der Seite
kein Icon angezeigt wird und wenn sich ein IconName ändert, ist es mühsam, sämtliche Stellen zu finden, an denen man es auch nachziehen muss.
Wenn der Compiler dann sofort einen Fehler werfen würde, wenn man eine Stelle vergessen hat, das wäre schon ein Träumchen.

Dem können wir abhilfe schaffen, indem wir ausgehend vom impliziten Typen dieser Konstanten einen Typen für die Iconnamen ableiten:

```typescript
type IconName = keyof typeof icons
```

Was machen wir hier? Mit typeof nehmen wir uns den Typen der Variable icons, mit keyof erhalten wir sämtliche keys, die dieses Objekt hat.
Und tatsächlich: Jetzt werden Tippfehler sofort gefunden und die IDE schlägt mir bei Verwendung sogar sofort die möglichen Werte vor.
```typescript
declare function showIcon(iconName: IconName)
```

Als nächstes möchten wir noch eine IconConfig anlegen, in der wir für jedes Icon optional noch zusätzliche Konfiugrationen hinterlegen können.

```typescript
type IconConfig = {
    color?: string
    size?: number
    hoverEffect?: boolean
}
```

Und auch hier möchten wir ein globales Objekt anlegen, in dem sämtliche IconConfigs gespeichert sind:

```typescript
const iconConfigs = {
    actionMovie: {color: 'red'},
    romanitcMovie: {size: 12, hoverEffect: true}
}
```

Und vielleicht ist es bereits jemandem aufgefallen, auch hier hat sich wieder ein Tippfehler eingeschlichen. Also sollten wir auch hierfür
einen Typen definieren, um das sofort zu bemerken:

```typescript
type IconConfigs = {
    [k in IconName]?: IconConfig
}
```

Und auch hier sehen wir jetzt sofort den Fehler. Außerdem schlägt die IDE uns wieder die möglcihen Keys vor.

Was ist das jetzt für ein Typ, der hier angelegt wurde? Das ist ein sogenannter Mapped Type.
Wir erzeugen ein Objekt und dessen Keys sind alle Werte des String-Union-Types IconName.
Jede dieser Properties machen wir dann optional (?) und weißen ihr den Typ IconConfig zu. Und Bam! Fertig.
Das sieht ziemlich ähnlich aus zu index signatures, die vielleicht der ein oder andere schon kennt

```typescript
type SomeOtherType = {
    [k: string]: IconConfig
}
```

Der Unterschied ist hier natürlich, dass wir eine endliche Menge an Keys haben (daher müssen wir bei mapped types auch explizit angeben,
wenn eine Property optional ist) und daher auch restriktiver und sicherer typisiert sind.

Übrigens kann man bei jedem Typ, der Properties hat (wozu ich auch Tupel zähle; die haben ja die Property `[0]` für den ersten 
Wert, `[1]` für den zweiten, etc.) mithilfe von Indexed Types auf diese dadurch spezifizierten Subtypen zugreifen.

Ich könnte also zum Beispiel sagen:

```typescript
type Person = {
    address: {
        street: string
        number: number
    }
    name: string
}

type Address = Person['address']

type MyTuple = [string, number]

let str: MyTuple[0] = 'Hi'
let num: MyTuple[1] = 12
```

Kommt hin und wieder vor, dass man das auch mal gebrauchen kann.

## Author als Type

Wir könnten das noch beliebig weiter führen. So könnten wir zum Beispiel sagen, wenn die Section vom Autor abgeleitet wird (Erster Buchstabe des Vornamens, erster Buxchstabe des Nachnamens),
und wir hätten eigene Typen für jeden Autor, dann könnten wir Book selbst generisch machen und an die Initialen herankommen mit:

```typescript
type Initials<T extends string> = T extends `${infer FIRST}${string} ${infer SECOND}${string}` ? `${FIRST}${SECOND}` : never
```

Die Section wäre dann auch generisch und hinge vom Autor ab. Wir benutzen den Utility Type UpperCase, den Typescript bietet, um von einem
übergebenen string literal type die Uppercase-Variante zu erhalten:

```typescript
type Author = string
type Section<AUTHOR extends Author> = Uppercase<Initials<AUTHOR>>
type BookId<AUTHOR extends Author> = `B-${Section<AUTHOR>}-${number}`

type Book<AUTHOR extends Authorf> = {
    title: string
    author: AUTHOR
    id: BookId<AUTHOR>
    section: Section<AUTHOR>
}
```

Kleines Beispiel:
```typescript
let book: Book<'Mark Twain'>
book = {
    title: 'Tom Sawyer',
    section: 'MT',
    author: 'Mark Twain',
    id: 'B-MT-34'
}
```

Wir sehen, dass die Werte für Author, Section und der erste Teil der ID bereits fest vorgegeben sind (die IDE schlägt sie auch vor).
Hier kommen wir allerdings an die Grenzen des Praktikablen. Denn wann hat man denn normalerweise pro Autor einen eigenen Typen? Insbesondere, wenn man die Daten nicht komplett selbst verwaltet,
sondern sie in eine Datenbank kommen, über eine API, eine Usereingabe, ... In all diesen Fällen kann man normalerweise nicht davon ausgehen, dass wir den Autor schon zur Compile-Zeit kennen.
Wir können zwar TypeGuards schreiben, aber eben auch nicht pro Autor; das ist dann zu viel des Guten.

Aber als Beispiel, wie es zum Beispiel aussehen könnte:

```typescript
function isMarkTwain(author: string): author is 'Mark Twain' {
    return author === 'Mark Twain'
}

function createBook(title: string, author: string, number: number): Book<string> {
    if (isMarkTwain(author)) {
        return {
            id: 'B-MT-123',
            section: 'MT',
            author,
            title
        } as Book<'Mark Twain'>
    }
    return null
}
```

Wenn wir den Return Type der isMarkTwain-Methode angucken, dann sehen wir, dass hier "author is Mark Twain" steht. Das
gibt an, dass es sich hier um einen Type Guard handelt. Wir geben TypeScript die Info, dass diese Methode einen
boolean zurückgibt. Ist der Wert 'true', dann ist der übergebene Parameter 'author' vom Typ 'Mark Twain', andernfalls nicht.

Wenn wir jetzt unten in die Methode gucken, ist author erst vom Typ string. Innerhalb des If-Blocks weiß TypeScript aber
wegen des TypeGuards, dass author vom Typ 'Mark Twain' sein muss, weil wir sonst nie hier hin gekommen wären.
Ergo kann ich author jetzt hier als 'Mark Twain' benutzen.


## Ende

Damit haben wir dann auch das nicht mehr praktikable, übertrieben Typisierte Beispiel erreicht, was das Ende dieses Vortrags bedeutet.

Die Beispiele mit AllCombinationsOf<AgeRating> und den Icons sind Fälle, die ich tatsächlich so (oder ähnlich) mit großem
Gewinn im Projekt einsetze. Die Book-Section-Geschichte ist dann leider zu unpraktikabel dazu.

Ich hoffe, ich konnte dem Ein oder Anderen TypeScript-Nutzer heute noch ein paar spannende und coole Features zeigen, die
vielleicth bald im Projetk Anwindung finden. Vielleicht habe ich jemandem, der noch nicht mit TypeScript gearbeitet hat,
auch Lust auf die Sprache gemacht. 

Zum Abschluss möchte ich hier noch ein paar Links teilen:

Einerseits die offizielle TypeScript-Doku: https://www.typescriptlang.org/docs/handbook/intro.html
Die ist echt gut geschrieben, mit vielen Beispielen und man findet zu allem, was ich heute gezeigt habe, etwas darin.

Dann die TypeScript Type Challenges: https://github.com/type-challenges/type-challenges
Das ist eine Sammlung an kleinen Aufgaben, in denen es darum geht, Typen zu definieren, die bestimmte Anforderungen erfüllen.
Kleine Knobelaufgaben von sehr leicht bis knackig schwer. Kann man direkt im Browser lösen und es sind sogar Testfälle dabei,
um direkt zu validieren, ob man die Aufgabe erfüllt hat.

Und dann ist da noch ein Repo, dass ich selbst mal für einen TypeScript-Type-Workshop angelegt hatte: TODO: Link
Mit weiteren kleinen Typ-Aufgaben. Angefangen von einfach bis immer komplexer.

Vielen Dank für Eure Aufmerksamkeit. Wenn es noch Fragen gibt, immer gerne, wir haben noch X minuten dafür.
Ansonsten, bei weiteren Fragen zum Vortrag, gerne auch per Mail. Meine Mailadresse steht hier an der Folie