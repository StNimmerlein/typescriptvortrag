## Einleitung

Willkommen zu meinem Vortrag zur Typisierung in TypeScript. Ich habe heute vor, euch in die Tiefen der
TypeScript-Möglichkeiten zu führen (und dort hoffentlich nicht zu verlieren). Ich zeige euch, was alles in TypeScript
drin steckt, warum es sich zu meiner liebsten Programmiersprache gemausert hat und wie auch ihr vielleicht eure Projekte
noch weiter ausgestalten könnt. Oder aber, wenn ihr noch nicht viel mit TypeScript gemacht habt, warum es sich lohnen
kann, mal einen zweiten Blick darauf zu werfen.

Auch für überwiegend Backendentwickler kommt hier hoffentlich der Anreiz oder wenigstens die Erkenntnis, dass man auch
im Frontend mittlerweile sauberen, typisierten Code schreiben kann. Und da sogar teilweise weiter gehen kann, als man es
sich in Sprachen wie Java nur erträumen kann. Wobei TypeScript ja auch immer häufiger im Backend eingesetzt wird. Gerade
Microservices oder Serverless bietet sich dazu einfach an. Aber das soll heute nicht das Thema sein, heute geht es
ausschließlich um die Typisierung.

# INFO
Über mich
Fragen am Ende (oder danach); wegen Zeit

## Syntax, erster Typ

- string, number, boolean, never, unknown, null, undefined, any
    - any meme
- type vs interface
    - same meme

```typescript
let a: string = 'Hallo'
type Movie = {
    id: string
    title: string
    ageRating: number
    genre: string
}
let movie: Movie
```

## Literal Types

```typescript
const text = 'Hallo'
```

## Union Types, Intersection Types

```typescript
let text2: 'Hallo' | 'Tschüss'
type ObjectWithId = { id: string }
type User = { username: string, age: number }
type UserInDb = User & ObjectWithId

type AgeRating = 0 | 6 | 12 | 16 | 18
type Genre = 'Drama' | 'Horror' | 'Romance'
```

## Template Literal Types

```typescript
type MovieId = `M-${AgeRating}-${number}`
// type BeginsWithDash = `-${string}`
// type EndsWithDash = `${string}-`
// type BeginsAndEndsWithDash = BeginsWithDash & EndsWithDash
// type BeginsOrEndsWithDash = BeginsWithDash | EndsWithDash
```

## Utility Types

```typescript
type ChildAgeRating = Exclude<AgeRating, 18 | 16>
type MovieWithoutId = Omit<Movie, 'id'>
```

## Generic und Conditional Types

```typescript
type LoaderState<T> = { state: 'loading' | 'error' | 'success', value: T }
```

```typescript
let a: 'hello' extends string ? true : number
a = true
a = 12
```

```typescript
type AsArray1<T> = T[]
type AsArray2<T> = T extends any ? T[] : never 
```

## Beispiel: Age Ratings

```typescript
function getAgeRatings(): AgeRating[] {
    return [0, 6, 16, 18]
}
```

```typescript
type AllCombinationsOf<T, S = T> = [T] extends [never]
    ? []
    : T extends S
        ? [T, ...AllCombinationsOf<Exclude<S, T>>]
        : never
```

## Mapped Types

```typescript
const icons = {
    horror: '<svg>...</svg>',
    drama: '<svg>...</svg>',
    romance: '<svg>...</svg>'
}

function showIcon(genre: Genre) {
    const icon = icons[genre]
}
```

## Ausblick

* Mapped types mit angepassten keys
* Inferred Types
```typescript
let icons: {
    [k in Genre as k extends 'Horror' ? never : Uncapitalize<k>]: string
}
type AgeRatingFromId<T extends MovieId> = T extends `M-${infer S}-${number}` ? S : never
```

## Ende

Damit haben wir dann auch das nicht mehr praktikable, übertrieben Typisierte Beispiel erreicht, was das Ende dieses Vortrags bedeutet.

Die Beispiele mit AllCombinationsOf<AgeRating> und den Icons sind Fälle, die ich tatsächlich so (oder ähnlich) mit großem
Gewinn im Projekt einsetze. Die Book-Section-Geschichte ist dann leider zu unpraktikabel dazu.

Ich hoffe, ich konnte dem Ein oder Anderen TypeScript-Nutzer heute noch ein paar spannende und coole Features zeigen, die
vielleicth bald im Projetk Anwindung finden. Vielleicht habe ich jemandem, der noch nicht mit TypeScript gearbeitet hat,
auch Lust auf die Sprache gemacht.

Zum Abschluss möchte ich hier noch ein paar Links teilen:

Einerseits die offizielle TypeScript-Doku: https://www.typescriptlang.org/docs/handbook/intro.html
Die ist echt gut geschrieben, mit vielen Beispielen und man findet zu allem, was ich heute gezeigt habe, etwas darin.

Dann die TypeScript Type Challenges: https://github.com/type-challenges/type-challenges
Das ist eine Sammlung an kleinen Aufgaben, in denen es darum geht, Typen zu definieren, die bestimmte Anforderungen erfüllen.
Kleine Knobelaufgaben von sehr leicht bis knackig schwer. Kann man direkt im Browser lösen und es sind sogar Testfälle dabei,
um direkt zu validieren, ob man die Aufgabe erfüllt hat.

Und dann ist da noch ein Repo, dass ich selbst mal für einen TypeScript-Type-Workshop angelegt hatte: TODO: Link
Mit weiteren kleinen Typ-Aufgaben. Angefangen von einfach bis immer komplexer.

Vielen Dank für Eure Aufmerksamkeit. Wenn es noch Fragen gibt, immer gerne, wir haben noch X minuten dafür.
Ansonsten, bei weiteren Fragen zum Vortrag, gerne auch per Mail. Meine Mailadresse steht hier an der Folie